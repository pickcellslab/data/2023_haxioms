
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SOX2", "TBXT", "SOX17", "LMBR"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds =["A1", "B1", "C1", "D1", "A2", "B2", "C2", "D2"]
conds = ["NA", "3µM", "4µM", "NA", "3µM", "0µM", "1µM", "2µM"]
#conds = ["A1_4µM", "B1_3µM", "C1_4µM", "D1_2µM", "A2_3µM", "0µM", "1µM", "D2_2µM"]


# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
times = [0]