
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SOX2", "TBXT", "SOX17", "LMBR"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds =["A1", "B1", "C1", "D1", "A2", "B2", "C2", "D2"]
conds = ["4µM", "2µM", "3µM", "4µM", "0µM", "1µM", "2µM", "3µM"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
times = [0]