
# Parameters for 220406_S2

# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["FOXA2", "TBXT", "SOX17"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds = ["B1","B2","B3", "B4"]
conds = ["0µM", "1µM","3µM","10µM"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
times = []
