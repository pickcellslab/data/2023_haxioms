## Fig 3C Notebook

Experiment description: SB dose response - cells were fixed 48h post CHIR and FGF induction and stained for SOX17 / SOX2 / TBXT

eLab entry: [CHIR and SB Titration V2](https://labbook.pickcellslab.org/experiments.php?mode=view&id=411)

