# Import data visualisation libraries
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import math
import pandas as pd



# Create a reusable function to generate boxplots of intensities
def boxplots_markers(df, markers, x_dim, x_ordr, hue_dim=None, hue_ordr=None, threshs=None, fig_dims=None) :

    # Adjust fonts
    plt.rcParams["axes.labelsize"] = 22
    sns.set(font_scale = 1.5)
    sns.set_style("ticks") # specifies that we want axes ticks to be shown


    # Create the figure for aligning the plots
    # first calculate the figure width:
    if(fig_dims==None): fig_dims = (len(markers)*len(x_dim),4)
    
    f, axs = plt.subplots(1, len(markers), figsize=fig_dims, sharey=False)


    # Create one plot per marker
    for b in range (len(markers)):
        # Initialize the figure with a logarithmic y axis
        axs[b].set_yscale("log")

        if(hue_dim == None):
            sns.boxplot(x=x_dim, y="Mean Intensity "+markers[b], hue=hue_dim, data=df, 
                    palette="vlag", order=x_ordr, hue_order=hue_ordr, ax=axs[b], showfliers = False).set(title=markers[b])
        else:
            sns.boxplot(x=x_dim, y="Mean Intensity "+markers[b], hue=hue_dim, data=df, 
                    palette="vlag", order=x_ordr, hue_order=hue_ordr, ax=axs[b], showfliers = False).set(title=markers[b])

        # Add in points to show each observation
        #sns.stripplot(x="Conditions", y="Mean Intensity "+markers[b], data=df, 
        #              size=2, color=".2", alpha=0.1, linewidth=0, ax=axs[b])

        # Tweak the visual presentation
        axs[b].set(xlabel="")
        axs[b].set(ylabel="Mean Intensity (A.U.)")
        axs[b].set_xticklabels(axs[b].get_xticklabels(),rotation = 40)

        # Add the threshold
        if(threshs != None):
            axs[b].axhline(threshs[b],0,1)

        if(hue_dim != None):
            if b == len(markers)-1:
                axs[b].legend(title="", bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)
            else:
                axs[b].legend([],[], frameon=False)


    f.tight_layout() # This fixes the spacing between individual plots
    
    


    
    

def scatters_3_markers(df, markers, x_column=None, x_categories=None, 
                       y_column=None, y_categories=None, threshs=None, c_mins=None, c_maxs=None,
                      alpha = 0.4, linear = False) :

    if(x_column == None):
        _scatters_3_markers_single(df, markers, threshs, c_mins, c_maxs, alpha, linear)
    elif(y_column == None):
        _scatters_3_markers_row(df, markers, x_column, x_categories, threshs, c_mins, c_maxs, alpha, linear)
    else:
        _scatters_3_markers_grid(df, markers, x_column, x_categories, y_column, y_categories, threshs, c_mins, c_maxs, alpha, linear)
    
    return

 


    
    
def _scatters_3_markers_single(df, markers, threshs=None, c_mins=None, c_maxs=None, alpha = 0.15, linear = False):
    
    current=df
    
    # Adjust fonts
    plt.rcParams["axes.labelsize"] = 22
    sns.set_style("ticks")
    sns.set(font_scale = 1.75)
    
    
    # utility indices
    xIndex = 0
    yIndex = 1
    cIndex = 2 # color


   
    # axes names
    x_name = "Mean Intensity " + markers[xIndex]
    y_name = "Mean Intensity " + markers[yIndex]

    # Build a color scale                
    colormap = sns.color_palette("magma", as_cmap=True)
    norm = plt.Normalize(c_mins[cIndex], c_maxs[cIndex])
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=norm)
    sm.set_array([])
   

    # Define the seaborn plot
    plot = sns.scatterplot(x=x_name, y=y_name, c=current["Mean Intensity " + markers[cIndex]], 
                               vmin=c_mins[cIndex], vmax=c_maxs[cIndex],
                               cmap=colormap, data=current, s=15, alpha=alpha, linewidth=0)


    # specify axes limits
    #plot.set_xlim(left=ax_mins[xIndex], right=ax_maxs[xIndex])
    #plot.set_ylim(bottom=ax_mins[yIndex], top=ax_maxs[yIndex])
    
    # Set a logarithmic x and y axes
    if(linear==False):
        plot.set(xscale="log")
        plot.set(yscale="log")

    # specify thresholds
    plot.axhline(threshs[yIndex], color=".4")
    plot.axvline(threshs[xIndex], color=".4")

    # Include percentages

    # NB: transform=ax.transAxes is to position text relative to plot rectangle
    ax = plot
    if len(current)!=0:
        to_count = current[(current[x_name]<threshs[xIndex]) & (current[y_name]>= threshs[yIndex])][x_name]
        percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
        plot.text(0.05, 0.9, percentage, transform=ax.transAxes)
        # bottom left
        to_count = current[(current[x_name]<threshs[xIndex]) & (current[y_name]< threshs[yIndex])][x_name]
        percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
        plot.text(0.05, 0.05, percentage, transform=ax.transAxes)
        # bottom right
        to_count = current[(current[x_name]>=threshs[xIndex]) & (current[y_name]< threshs[yIndex])][x_name]
        percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
        plot.text(0.65, 0.05, percentage, transform=ax.transAxes)
        # upper right
        to_count = current[(current[x_name]>=threshs[xIndex]) & 
                               (current[y_name]>= threshs[yIndex])][x_name]
        percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
        plot.text(0.65, 0.9, percentage, transform=ax.transAxes)



    # Add the color scale on the right hand side if we have last column                

        cbar = plot.figure.colorbar(sm)
        cbar.set_ticks([]) #removes tick to gain space
        cbar.ax.get_yaxis().labelpad = 30
        cbar.ax.set_ylabel(markers[cIndex], rotation=270)
    

    
    

def _scatters_3_markers_row(df, markers, x_column, x_categories, threshs=None, c_mins=None, c_maxs=None, alpha = 0.15, linear = False):
    
    # Adjust fonts
    plt.rcParams["axes.labelsize"] = 22
    sns.set_style("ticks")
    sns.set(font_scale = 1.75)
    
    
    # utility indices
    xIndex = 0
    yIndex = 1
    cIndex = 2 # color
    column = 0;

    #Initialise the plots
    f, axs = plt.subplots(1, len(x_categories), figsize=(len(x_categories)*5, 5), sharex=True, sharey=True)
    
    # Initialize the figure with a logarithmic x and y axes
    if(linear==False):
        axs[column].set_xscale("log")
        axs[column].set_yscale("log")

    # axes names
    x_name = "Mean Intensity " + markers[xIndex]
    y_name = "Mean Intensity " + markers[yIndex]

    # Build a color scale                
    colormap = sns.color_palette("magma", as_cmap=True)
    norm = plt.Normalize(c_mins[cIndex], c_maxs[cIndex])
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=norm)
    sm.set_array([])
   

    for t in x_categories:

        current = df.loc[(df[x_column]==t)]

        # Define the seaborn plot
        plot = sns.scatterplot(x=x_name, y=y_name, c=current["Mean Intensity " + markers[cIndex]], 
                                   vmin=c_mins[cIndex], vmax=c_maxs[cIndex],
                                   cmap=colormap, data=current, s=15, alpha=alpha, linewidth=0, ax=axs[column])


        # specify axes limits
        #plot.set_xlim(left=ax_mins[xIndex], right=ax_maxs[xIndex])
        #plot.set_ylim(bottom=ax_mins[yIndex], top=ax_maxs[yIndex])

        # specify thresholds
        plot.axhline(threshs[yIndex], color=".4")
        plot.axvline(threshs[xIndex], color=".4")

        # Include percentages

        # NB: transform=ax.transAxes is to position text relative to plot rectangle
        # upper left
        if len(current)!=0:
            to_count = current[(current[x_name]<threshs[xIndex]) & (current[y_name]>= threshs[yIndex])][x_name]
            percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
            plot.text(0.05, 0.9, percentage, transform=axs[column].transAxes)
            # bottom left
            to_count = current[(current[x_name]<threshs[xIndex]) & (current[y_name]< threshs[yIndex])][x_name]
            percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
            plot.text(0.05, 0.05, percentage, transform=axs[column].transAxes)
            # bottom right
            to_count = current[(current[x_name]>=threshs[xIndex]) & (current[y_name]< threshs[yIndex])][x_name]
            percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
            plot.text(0.65, 0.05, percentage, transform=axs[column].transAxes)
            # upper right
            to_count = current[(current[x_name]>=threshs[xIndex]) & 
                                   (current[y_name]>= threshs[yIndex])][x_name]
            percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
            plot.text(0.65, 0.9, percentage, transform=axs[column].transAxes)


        # Add x_category as title
        axs[column].set(title = t)

        # Add the color scale on the right hand side if we have last column                
        if column == len(x_categories)-1:
            cbar = f.colorbar(sm, ax=axs[column])
            cbar.set_ticks([]) #removes tick to gain space
            cbar.ax.get_yaxis().labelpad = 30
            cbar.ax.set_ylabel(markers[cIndex], rotation=270)


        # Now update indices
        #print(c, column, row)
        column = column + 1     


    f.tight_layout()










    

    
def _scatters_3_markers_grid(df, markers, x_column, x_categories, y_column, y_categories, threshs=None, c_mins=None, c_maxs=None,alpha = 0.4, linear = False):
    
    
    # Adjust fonts
    plt.rcParams["axes.labelsize"] = 50
    sns.set_style("ticks")
    sns.set(font_scale = 2)
    
    
    # utility indices
    xIndex = 0
    yIndex = 1
    cIndex = 2 # color
    column = 0;
    row = 0;

    #Initiate the plots
    f, axs = plt.subplots(len(y_categories), len(x_categories), figsize=(len(x_categories)*5, len(y_categories)*5 + 3), sharex=True, sharey=True)
    
    if(linear==False):
        # Initialize the figure with a logarithmic x and y axes
        axs[row][column].set_xscale("log")
        axs[row][column].set_yscale("log")

    # axes names
    x_name = "Mean Intensity " + markers[xIndex]
    y_name = "Mean Intensity " + markers[yIndex]

    # Build a color scale                
    colormap = sns.color_palette("magma", as_cmap=True)
    norm = plt.Normalize(c_mins[cIndex], c_maxs[cIndex])
    sm = plt.cm.ScalarMappable(cmap=colormap, norm=norm)
    sm.set_array([])


    for s in y_categories:
        for t in x_categories:

            current = df.loc[(df[x_column]==t) & (df[y_column]==s)]

            # Define the seaborn plot
            plot = sns.scatterplot(x=x_name, y=y_name, c=current["Mean Intensity " + markers[cIndex]], 
                                       vmin=c_mins[cIndex], vmax=c_maxs[cIndex],
                                       cmap=colormap, data=current, s=15, alpha=alpha, linewidth=0, ax=axs[row][column])




            # specify axes limits
            #plot.set_xlim(left=ax_mins[xIndex], right=ax_maxs[xIndex])
            #plot.set_ylim(bottom=ax_mins[yIndex], top=ax_maxs[yIndex])

            # specify thresholds
            plot.axhline(threshs[yIndex], color=".4")
            plot.axvline(threshs[xIndex], color=".4")

            # Include percentages

            # NB: transform=ax.transAxes is to position text relative to plot rectangle
            # upper left
            if len(current)!=0:
                to_count = current[(current[x_name]<threshs[xIndex]) & (current[y_name]>= threshs[yIndex])][x_name]
                percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
                plot.text(0.05, 0.9, percentage, transform=axs[row][column].transAxes)
                # bottom left
                to_count = current[(current[x_name]<threshs[xIndex]) & (current[y_name]< threshs[yIndex])][x_name]
                percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
                plot.text(0.05, 0.05, percentage, transform=axs[row][column].transAxes)
                # bottom right
                to_count = current[(current[x_name]>=threshs[xIndex]) & (current[y_name]< threshs[yIndex])][x_name]
                percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
                plot.text(0.65, 0.05, percentage, transform=axs[row][column].transAxes)
                # upper right
                to_count = current[(current[x_name]>=threshs[xIndex]) & 
                                       (current[y_name]>= threshs[yIndex])][x_name]
                percentage = '{:.2f}%'.format(len(to_count) * 100 / len(current))
                plot.text(0.65, 0.9, percentage, transform=axs[row][column].transAxes)


            # Add x_category as title if first row
            if row == 0:
                axs[row][column].set(title = t)

            # Add the color scale on the right hand side if we have last column                
            if column == len(x_categories)-1:
                cbar = f.colorbar(sm, ax=axs[row][column])
                cbar.set_ticks([]) #removes tick to gain space
                cbar.ax.get_yaxis().labelpad = 30
                cbar.ax.set_ylabel(markers[cIndex], rotation=270)


            # Now update indices
            #print(c, column, row)
            column = column + 1
            if column == len(x_categories) : row = row + 1        
            column = column % len(x_categories)
            row = row % len(y_categories)       


    f.tight_layout()
    
    
    
    
def grouped_barplots(bars, x_dim, y_dims, hue_dim=None):
    
    sns.set(font_scale = 1.5)
    
    if(len(y_dims)==1):
        _grouped_barplot_single(bars, x_dim, y_dims[0], hue_dim)
    else:
        _grouped_barplot_several(bars, x_dim, y_dims, hue_dim)
    
    
    
    
    
def _grouped_barplot_single(bars, x_dim, y_dim, hue_dim):
    
        plot = sns.barplot(x=x_dim, y=y_dim, hue=hue_dim, data=bars)
        swarm = sns.stripplot(x=x_dim, y=y_dim, hue=hue_dim, data=bars,  dodge=True, linewidth=1)
        #sns.barplot(x="Times", y=y_dims[m], data=bars, ax=axs[m])color=".2", 

        #plot.set(ylabel="% of "+y_dim+" positive cells")
        plot.set(xlabel="")
         #Rotate labels
        plot.set_xticklabels(plot.get_xticklabels(),rotation = 90)
        
        # Now customise the legend
        handls, labls = plot.get_legend_handles_labels()
        i = int(len(handls)/2)
        j = len(handls)
        
        plot.legend(title="", borderaxespad=0, handles= handls[i:j], labels = labls[i:j])
        
       
        
        
        
    
    
    
def _grouped_barplot_several(bars, x_dim, y_dims, hue_dim):
    
    #Initiate the plots
    f, axs = plt.subplots(1, len(y_dims), figsize=(len(y_dims)*5, 5), sharey=True)

    for m in range(0,len(y_dims)):
        
        plot = sns.barplot(x=x_dim, y=y_dims[m], hue=hue_dim, data=bars, ax=axs[m])
        swarm = sns.stripplot(x=x_dim, y=y_dims[m], hue=hue_dim, data=bars,  dodge=True, linewidth=1, ax=axs[m])
        #sns.barplot(x="Times", y=y_dims[m], data=bars, ax=axs[m])color=".2", 

        #plot.set(ylabel="% of "+y_dims[m])
        plot.set(xlabel="")
        
        # Now customise the legend
        handls, labls = axs[m].get_legend_handles_labels()
        i = int(len(handls)/2)
        j = len(handls)
        
        axs[m].legend(title="", borderaxespad=0, handles= handls[i:j], labels = labls[i:j])
        #axs[m].legend(title="", bbox_to_anchor=(1.02, 1), loc='upper left', borderaxespad=0)  loc='upper left',

    f.tight_layout()
        
        
    
    
def stacked_barplots(df, x_dim, graph_dim=None, states_order=None):
    
    if(graph_dim==None): _stacked_barplot_single(df, x_dim, states_order)
    else: _stacked_barplot_several(df, x_dim, graph_dim, states_order)
    
    
    
    
def _stacked_barplot_single(df, x_dim, states_order=None):
    
    x_categories = df[x_dim].unique()
    stack = pd.crosstab(index=df[x_dim], columns=df['State'], normalize="index")
    
    #reorder columns if states_order non null
    if(states_order!=None) : stack=stack.reindex(columns=states_order)
    
    plot = stack.plot(kind='bar', stacked=True)

    plot.set(xlabel="")

    plot.legend(title="", loc='upper left', bbox_to_anchor=(1.02, 1), borderaxespad=0)
       
        
        
        
        
        
def _stacked_barplot_several(df, x_dim, graph_dim,states_order):
    
    x_categories = df[x_dim].unique()
    graph_categories = df[graph_dim].unique()
    
    f, axs = plt.subplots(1, len(graph_categories), figsize=(len(graph_categories)*5, 5), sharey=True)

    col=0;
    for s in graph_categories:
        
        current = df.loc[(df[graph_dim]==s)]
        stack = pd.crosstab(index=current[x_dim], columns=current['State'], normalize="index")
        
        #reorder columns if states_order non null
        if(states_order!=None) : stack=stack.reindex(columns=states_order)

        stack.plot(kind='bar', stacked=True, ax=axs[col])

        axs[col].set(xlabel="")
        axs[col].set(title = s)

        if(col == len(graph_categories)-1):
            axs[col].legend(title="", loc='upper left', bbox_to_anchor=(1.02, 1), borderaxespad=0)
        else:
            axs[col].legend([],[], frameon=False)

        col += 1
