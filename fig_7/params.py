
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SMAD2/3", "LEF1", "LMBR"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds =["_0h", "CTRL", "_L0_", "_L24_", "_S0_", "_S24_", "_SL0_", "_SL24_", "_4um"]
conds = ["t0h", "CTRL", "L0", "L24", "S0", "S24", "SL0", "SL24", "4µM"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = ["_0h", "_12h", "_24h", "_36h", "_48h"]
times = [0, 12, 24, 36, 48]
#NB if using T0h and another time with 0 in it eg. 60h, then write the 0h as in the image eg _0h. If not the second timepoint with 0 will be counted within the T0h and it wont seperate them as it should