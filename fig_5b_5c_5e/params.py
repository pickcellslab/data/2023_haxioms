
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SMAD23", "LEF1", "LMBR"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds =["A1", "A2", "A3", "A4", "B1", "B2", "B3", "B4"]
conds = ["0µM", "1µM", "2µM", "3µM", "4µM",  "SL@0h" , "SL@24h", "4µM"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = ["_0h", "_6h", "12h", "24h", "36h", "48h"]
times = [0, 6 ,12, 24, 36, 48]
#NB if using T0h and another time with 0 in it eg. 60h, then write the 0h as in the image eg _0h. If not the second timepoint with 0 will be counted within the T0h and it wont seperate them as it should