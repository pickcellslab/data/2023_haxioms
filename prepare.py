import numpy as np
import pandas as pd
from algebra import *


def parse(table, params):
    """
    Import and rearranges a data table generated with PickCells
    :param str table: The name of the data table file (including extension if there is one"
    :param module params: the parameter file as an imported module
    :return: the data in the form of a panda dataframe
    """
    # Read the data as a pandas frame
    df = pd.read_csv(table, sep='\t')

    # retain only the columns we are interested in
    # This includes the mean intensity of all markers defined in params
    col_list = ['Image Name', 'Normalised Coordinate 0', 'Normalised Coordinate 1', 'Normalised Coordinate 2','volume']
    
    for m in params.markers:
        col_list.append('Mean Intensity ' + m)

    #Convert to Tuple
    cols = (* col_list,)
    
    # now drop all unnecessary columns
    df = df.loc[:,df.columns.intersection(cols)]

    # Rename columns to more manageable names
    if 'Normalised Coordinate 0' in df:	df.rename(columns={'Normalised Coordinate 0': 'X'}, inplace=True)
    else : df.rename(columns={'Centroid X': 'X'}, inplace=True)
    if 'Normalised Coordinate 1' in df:	df.rename(columns={'Normalised Coordinate 1': 'Y'}, inplace=True)
    else : df.rename(columns={'Centroid Y': 'Y'}, inplace=True)
    if 'Normalised Coordinate 2' in df:	df.rename(columns={'Normalised Coordinate 2': 'Z'}, inplace=True)
    else : df.rename(columns={'Centroid Z': 'Z'}, inplace=True)


    # create a new column to specify conditions
    if len(params.image_conds) != 0:
        predicates = [(df['Image Name'].str.contains(x)) for x in params.image_conds]
        df['Conditions'] = np.select(predicates, params.conds)
    
    # create a new column to specify time points
    if len(params.image_times) != 0:
        predicates = [(df['Image Name'].str.contains(x)) for x in params.image_times]
        df['Times'] = np.select(predicates, params.times)
    
    # create a new column to specify geometries
    if len(params.image_shapes) != 0:
        predicates = [(df['Image Name'].str.contains(x)) for x in params.image_shapes]
        df['Shapes'] = np.select(predicates, params.shapes)

    return df




def assign_conditions(df, params):

    # create a new column to specify conditions
    if len(params.image_conds) != 0:
        predicates = [(df['Image Name'].str.contains(x)) for x in params.image_conds]
        df['Conditions'] = np.select(predicates, params.conds)
    
    # create a new column to specify time points
    if len(params.image_times) != 0:
        predicates = [(df['Image Name'].str.contains(x)) for x in params.image_times]
        df['Times'] = np.select(predicates, params.times)
    
    # create a new column to specify geometries
    if len(params.image_shapes) != 0:
        predicates = [(df['Image Name'].str.contains(x)) for x in params.image_shapes]
        df['Shapes'] = np.select(predicates, params.shapes)

    return df





def experimental_design(df):
    
    if "Conditions" in df:
        print('\nUnique Conditions:')
        print(pd.unique(df['Conditions']))
    if "Times" in df:
        print('\nUnique Times:')
        print(pd.unique(df['Times']))
    if "Shapes" in df:
        print('\nUnique Shapes:')
        print(pd.unique(df['Shapes']))

        
        
def cell_and_colony_count(df, groups):
    
    appended = groups.copy()
    appended.append("Image Name")
    
    group1 = df.groupby(appended)["volume"].count().reset_index()
    group1["Image Name"]=1

    group2 = group1.groupby(groups)[["Image Name","volume"]].sum().reset_index()
    group2.rename(columns = {'Image Name':'Colonies', 'volume':'cells'}, inplace = True)
    print(group2)  
    
    
    
    
    

        
def assign_cell_identities(df, markers, threshs):
    
    # Adds one column per marker and set a boolean value based on whether intensity is above thresh or not
    for m in range(0,len(markers)):
        df[markers[m]] = False
        df.loc[df["Mean Intensity "+markers[m]] > threshs[m],markers[m]] = True
    
    # Now add a "State" column 
    if(len(markers)==3):
        predicates = [
        df[markers[0]].eq(True) & df[markers[1]].eq(True) & df[markers[2]].eq(True),
        df[markers[0]].eq(True) & df[markers[1]].eq(True) & df[markers[2]].eq(False),
        df[markers[0]].eq(True) & df[markers[1]].eq(False) & df[markers[2]].eq(False),
        df[markers[0]].eq(True) & df[markers[1]].eq(False) & df[markers[2]].eq(True),
        df[markers[0]].eq(False) & df[markers[1]].eq(False) & df[markers[2]].eq(True),
        df[markers[0]].eq(False) & df[markers[1]].eq(True) & df[markers[2]].eq(True),
        df[markers[0]].eq(False) & df[markers[1]].eq(True) & df[markers[2]].eq(False),
        df[markers[0]].eq(False) & df[markers[1]].eq(False) & df[markers[2]].eq(False)
        ]

        choices = [
            "Triple+",
            markers[0]+"+/"+markers[1]+"+",
            markers[0],
            markers[0]+"+/"+markers[2]+"+",
            markers[2],
            markers[1]+"+/"+markers[2]+"+",
            markers[1],
            "Negative"
        ]
    elif(len(markers)==2):
        predicates = [
        df[markers[0]].eq(True) & df[markers[1]].eq(True) ,
        df[markers[0]].eq(True) & df[markers[1]].eq(False) ,            
        df[markers[0]].eq(False) & df[markers[1]].eq(True) ,
        df[markers[0]].eq(False) & df[markers[1]].eq(False)
        ]

        choices = [
            "Double+",
            markers[0],
            markers[1],
            "Negative"
        ]

    df['State'] = np.select(predicates, choices, default="Negative")
    
        


def add_edge_dist(df):

    """
    Adds a column to the dataframe to include the distance of each object from the border of the group
    :param df: The dataframe containing the data (generated from parse())
    :return: the dataframe with the added column 'edge_dist'
    """

# Compute the distance of each nucleus from the border of the group

# First create a new column in df where we will add the distance to the periphery of the group

    df.loc[:,'edge_dist'] = -1

    # Iterate over all unique image names and compute the convex hull
    # as well as the distance of each nucleus from the convex hull
    for image in df['Image Name'].unique():

        # Select all rows belonging to the current image
        colony = df.loc[df['Image Name']==image, 'X': 'Y']
        # make a numpy version to simplify below code
        points = colony[['X', 'Y']].to_numpy()

        # Compute the convex hull
        hull = ConvexHull(points)

        pt_dist = []
        for p_idx in range(len(points)):
            pt = points[p_idx,:]
            dist_list = []
            for v_idx in range(len(hull.vertices)):
                v1 = hull.vertices[v_idx - 1]
                v2 = hull.vertices[v_idx]
                start = points[v1]
                end = points[v2]
                temp = pnt2line(pt, start, end)
                dist_list.append(temp[0])

            #Check point is within polygon
            inside =  point_in_poly(pt[0],pt[1],points[hull.vertices])
            if (inside == True):
                dist_temp = -1. * min(dist_list)
            else:
                dist_temp = min(dist_list)          

            # now place back into df using indices in colony
            df.at[colony.index[p_idx], 'edge_dist'] = -dist_temp

    # finally sort in ascending distance from edge
    df = df.sort_values("edge_dist")
    
    return df





def add_centre_dist(df):  
    """
    Adds a column to the dataframe to include the distance of each object from the centre of the group
    :param df: The dataframe containing the data (generated from parse())
    :return: the dataframe with the added column 'centre_dist'
    """
    df["centre_dist"]= np.nan
    for i in range(df.shape[0]):
        df.at[i,"centre_dist"] = math.sqrt(df["X"][i]*df["X"][i] + df["Y"][i]*df["Y"][i])
    df = df.sort_values("centre_dist")
    return df



def create_bins(df, column, num_bin):
    
    bins = np.linspace(df[column].min(), df[column].max(), num_bin)
    bins = bins[::-1]


    #Get the indices of the bins to which each value in input array belongs
    indices = np.digitize(df[column], bins)
    df[column+'_bins'] = bins[indices]
    
    


def add_norm_centre_dist(df, params):  
    """
    Adds a column to the dataframe to include the normalised distance of each object from the centre of the group
    :param df: The dataframe containing the data (generated from parse())
    :param module params: the parameter file as an imported module 
    :return: the dataframe with the added column 'centre_dist'
    """
    
    # Create the column
    df["norm_centre_dist"]= np.nan
                     
    # Create a dictionary for fast access
    # di = dict(zip(params.conds, params.radii))                 
                     
    for i in range(df.shape[0]):
        n = df.loc[i,"centre_dist"]
        mx = int(df.loc[i,"Shapes"])
        df.loc[i,"norm_centre_dist"] = n/mx           
                     
    df = df.sort_values("norm_centre_dist")
                     
    return df
