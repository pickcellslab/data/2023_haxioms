## Fig 1E Notebook

Experiment description: Documents the patterning of SOX2/TBXT/FOXA2 markers in 500uM discs fixed at 48h

eLab entry: [20231011 - Timecourse Analysis 2µM hAXIOMS 0h - 60h](https://labbook.pickcellslab.org/experiments.php?mode=view&id=476 (231011_0_60h_timecourse - Staining 2)


NB: The python notebook provided here includes the analysis of the FOXA2/TBXT/SOX17 staining at 48h which was part of a larger experiment including additional timepoints and stainings IF shown in Fig 2B and supplementary XX.
