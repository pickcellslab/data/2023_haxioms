
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["FOXA2", "TBXT", "SOX17", "LMBR"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds =["A2","B1"]
conds = ["2µM CHIR", "2µM CHIR"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = ["48"]
times = ["48h"]