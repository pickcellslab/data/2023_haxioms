
# Parameters for 220406_S1

# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SOX17", "TBXT", "FOXA2"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds = ["A1","A2","A3", "A4","B1"]
conds = ["0µM", "1µM","2µM","3µM", "4µM"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
times = []
