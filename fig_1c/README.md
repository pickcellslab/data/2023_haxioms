## Jupyter Notebook: Fig 1C and Supplementary Figure 2

Experiment description: SOX17, TBXT, SOX2 staining in micropattern colonies of increasing diameter; fixed at 48h post 2µM CHIR and 20ng/ml FGF induction in N2B27 medium.

eLab entry: [hAXIOMs Scaling CHIR vs WNT3](https://labbook.pickcellslab.org/experiments.php?mode=view&id=319)


