
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SOX2", "TBXT", "SOX17"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds = ["A1","A2","A3","A4","B1","B2","B3","B4"]
conds = ["120","220","320","420","520","620","720","820"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
times = []
