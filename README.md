
# Description

This repository contains scripts, data and notebooks to reproduce the figures of our preprint "In vitro modelling of anterior primitive streak patterning with hESC reveals the dynamic of WNT and NODAL signalling required to specify notochord progenitors"
Miguel Robles Garcia, Chloe Thimonier, Konstantina Angoura, Ewa Ozga, Heather MacPherson, Guillaume Blin
bioRxiv 2023.06.01.543323; doi: [https://doi.org/10.1101/2023.06.01.543323](https://doi.org/10.1101/2023.06.01.543323 ) 


# How to use

  * Clone the repository
  * cd into the desired figure directory
  * Launch Jupyter Lab or RStudio accordingly
  * Run the notebook


