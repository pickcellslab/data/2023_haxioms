
# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["OCT4","SOX2"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
#image_conds = ["A1","A2","A3", "A4", "B1", "B2", "B3", "B4"]
#conds = ["Control","U129 3µM","U129 10µM", "U129 30µM", "DMSO", "SB 1µM", "SB 3µM", "SB 10µM" ]

image_conds = ["B1"]
conds = ["Control"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
times = []
