## Fig 1D Notebook

Experiment description: OCT4, SOX2 staining in micropattern colonies of 520µm diameter fixed at 48h post CHIR and FGF induction in N2B27 medium.

eLab entry: [hAXIOMs with RHOKi and IWP 2 and UniPR (vol 3)](https://labbook.pickcellslab.org/experiments.php?mode=view&id=330)
NB: only B1 well was used here
