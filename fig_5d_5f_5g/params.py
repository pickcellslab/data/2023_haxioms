# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["CER1", "LEFTY2", "NODAL", "DAPI"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds = ["0uM","1uM","2uM","3uM","4uM","SL0h","SL24h"]
conds = ["0µM", "1µM", "2µM", "3µM", "4µM", "SL@0h", "SL@24h"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = ["0h_", "^6h_", "12h_", "24h_", "36h_", "48h_"]
#times = ["0h", "6h", "12h", "24h", "36h", "48h"]
times = [0, 6, 12, 24, 36, 48]