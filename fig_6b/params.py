# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["NOTO", "CHRD", "NODAL"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds = ["_Ctrl","_SB0h","_SB24h","_LDN24h_","_LDN0hSB24h","_LDN24hSB0h","_SBLDN0h","_SBLDN24h"]
conds = ["Ctrl","S0h","S24h","L24h","S24h L0h","S0h L24h","SL0h","SL24h"]

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = []
#times = ["0h", "6h", "12h", "24h", "36h", "48h"]
times = []