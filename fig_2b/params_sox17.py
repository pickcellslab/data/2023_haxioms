
# Parameters for 220406_S1

# define markers 
# Note that by convention the order should the same as X, Y and Z in scatterplots of intensities
markers = ["SOX2", "TBXT", "SOX17", "LMBR"]

# The following lists are used to map conditions/time points and geometries
# to labels in the image names

# Conditions
image_conds =[]
conds = []

# Shapes
image_shapes = []
shapes = []

# Time points
image_times = ["_0h", "12h", "24h", "36h", "48", "60h"]
times = [0, 12, 24, 36, 48, 60]
#NB if using T0h and another time with 0 in it eg. 60h, then write the 0h as in the image eg _0h. If not the second timepoint with 0 will be counted within the T0h and it wont seperate them as it should